package com.pawelbanasik.springdemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component("myorg")
public class Organization {

	@Value("${companyName}")
	private String companyName;
	
	@Value("${yearOfIncorporation}")
	private int yearOfIncorporation;
	
	private String postalCode;
	private String employeeCount;
	
	public Organization() {
	}

	public Organization(String companyName, int yearOfIncorporation) {
		this.companyName = companyName;
		this.yearOfIncorporation = yearOfIncorporation;
	}

	public void corporateSlogan() {
		String slogan = "We build the ultimate driving machines";
		System.out.println(slogan);
	}

	@Override
	public String toString() {
		return "Organization [companyName=" + companyName + ", yearOfIncorporation=" + yearOfIncorporation
				+ ", postalCode=" + postalCode + ", employeeCount=" + employeeCount + "]";
	}

	@Autowired
	public void setEmployeeCount(@Value("${employeeCount}")String employeeCount) {
		this.employeeCount = employeeCount;
	}

	@Autowired
	public void setPostalCode(@Value("${postalCode}")String postalCode) {
		this.postalCode = postalCode;
	}
}
